#!/bin/bash

# Official instrucions: https://gitlab.com/RZZT/thm-jekyll.git -> Method 2

sudo apt-get install ruby ruby-dev make gcc
sudo gem install jekyll bundler
bundle install
