---
title: Confidential
subtitle: We got our hands on a confidential case file from some self-declared "black hat hackers"... it looks like they have a secret invite code.
tags:
- Linux
- pdfinfo
- exiftool
- pdfimages
thumbnail-img: ../../assets/img/confidential/confidential1.png
---

![](../../assets/img/confidential/confidential1.png){: .mx-auto.d-block :}

This is a **free** [room](https://tryhackme.com/room/confidential) from [TryHackMe](https://tryhackme.com), created by [cmnatic](https://tryhackme.com/p/cmnatic).

{: .box-warning}

**Disclaimer:** No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️

# Setting-up the Environment

First of all, we are going to connect to the **VPN** using my custom [script](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/vpn.sh) that I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```java
❯ which vpn
vpn: aliased to sh ~/dotfiles/scripts/vpn.sh

❯ vpn thm
```

Then, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/thm.sh) to add the *IP* address and **hostname** of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. 

```java
❯ which thm
thm: aliased to sh ~/dotfiles/scripts/thm.sh
```

We will run the script and write the information of this machine.

```java
❯ thm
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.10.147.153
[?] Type the name of the box: Confidential

[!] Try Hack Me Machine
10.10.147.153 Confidential.thm

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.10.147.153             

[!] Working directory created
[!] Initial configuration completed
```

# 1. LibreOffice Draw

In the `AttackBox` we will get a **partial** **QR** code for investigation.

![](../../assets/img/confidential/confidential2.png){: .mx-auto.d-block :}

This is the `.pdf` file.

![](../../assets/img/confidential/confidential3.png){: .mx-auto.d-block :}

The first thing we can try is to open it with **LibreOffice Draw** to see if we can **edit** it.

![](../../assets/img/confidential/confidential4.png){: .mx-auto.d-block :}

With **LibreOffice Draw** we can move the image covering the **QR code** easily.

![](../../assets/img/confidential/confidential5.png){: .mx-auto.d-block :}

# 2. Commands and tools

Another more complicated way is to use **Linux** **commands** and **tools**. The first thing to do is to **transfer** the `PDF file` to our computer with the following command.

```python
ubuntu@thm-confidential:~/confidential$ python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

In our **computer** we will download the file with this command.

```python
❯ wget http://10.10.147.153:8000/Repdf.pdf
```

## 2.1 pdfinfo

We will search for information with the `pdfinfo` tool but we will not see anything interesting.

```python
❯ pdfinfo Repdf.pdf
Producer:        cairo 1.17.4 (https://cairographics.org)
CreationDate:    Sat Feb  5 14:01:35 2022 CET
Custom Metadata: no
Metadata Stream: no
Tagged:          no
UserProperties:  no
Suspects:        no
Form:            none
JavaScript:      no
Pages:           1
Encrypted:       no
Page size:       849.894 x 1099.86 pts
Page rot:        0
File size:       102818 bytes
Optimized:       no
PDF version:     1.5
```

## 2.2 exiftool

With [exiftool](https://www.kali.org/tools/libimage-exiftool-perl/) we won't find anything either.

```python
❯ exiftool Repdf.pdf
ExifTool Version Number         : 12.44
File Name                       : Repdf.pdf
Directory                       : .
File Size                       : 103 kB
File Modification Date/Time     : 2022:03:11 21:56:29+01:00
File Access Date/Time           : 2022:08:23 16:54:13+02:00
File Inode Change Date/Time     : 2022:08:23 16:54:05+02:00
File Permissions                : -rw-r--r--
File Type                       : PDF
File Type Extension             : pdf
MIME Type                       : application/pdf
PDF Version                     : 1.5
Linearized                      : No
Page Count                      : 1
Producer                        : cairo 1.17.4 (https://cairographics.org)
Create Date                     : 2022:02:05 18:31:35+05:30
```

## 2.3 pdfimages

Finally we will use [pdfimages](https://www.cyberciti.biz/faq/easily-extract-images-from-pdf-file/) to **extract all** the images and we will be able to see the **QR code**.

```python
❯ pdfimages -all Repdf.pdf images
❯ ls
images-000.png  images-001.png  images-002.png  Repdf.pdf
```

This is the image that **covered** the **QR code**.

![](../../assets/img/confidential/confidential6.png){: .mx-auto.d-block :}

To **extract** the **flag** from the **QR code** I have used this web site, just **upload** the image with the `uncovered` **QR code**.

- [https://www.online-qr-scanner.com/](https://www.online-qr-scanner.com/)

![](../../assets/img/confidential/confidential7.png){: .mx-auto.d-block :}

I hope you made it too, see you next timee! 🙃
