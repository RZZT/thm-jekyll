---
title:
subtitle:
tags:
- Linux
- 
thumbnail-img: assets/img//1.png
---

![](../../assets/img//1.png){: .mx-auto.d-block :}

This is a **free** [room](https://tryhackme.com/room/) from [TryHackMe](https://tryhackme.com), created by .

{: .box-warning}

**Disclaimer:** No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️

# Setting-up the Environment

First of all, we are going to connect to the **VPN** using my custom [script](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/vpn.sh) that I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```java
❯ which vpn
vpn: aliased to sh ~/dotfiles/scripts/vpn.sh

❯ vpn thm
```

Then, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/thm.sh) to add the *IP* address and **hostname** of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. 

```java
❯ which thm
thm: aliased to sh ~/dotfiles/scripts/thm.sh
```

We will run the script and write the information of this machine.

```java

```

# 1. Enumeration

Now we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```java

```

To identify which operating system is the target running. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool.

```java

```

{: .box-note}

By default Linux has a TTL of 64 and Windows has 128.

## 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

- [-p-](https://nmap.org/book/nmap-overview-and-demos.html) : scan every TCP port [0-65535].
- [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
- [-sS](https://nmap.org/book/man-port-scanning-techniques.html) : set TCP SYN scan.
- [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
- [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.
- [-n](https://nmap.org/book/host-discovery-dns.html) : No DNS resolution.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- {name}.thm : Target machine.
- [-oG](https://nmap.org/book/output-formats-grepable-output.html) : Grepeable output.
- allPorts : Output file name.

```java

```

This is the function that I have defined in the [zsh](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) configuration file to extract TCP ports.

```python
❯ which extractPorts
extractPorts () {
    ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')" 
    ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)" 
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"
    echo -e "\t${grayColour}[*] IP Address:${endColour} ${greenColour} $ip_address${endColour}"
    echo -e "\t${grayColour}[*] Open ports:${endColour} ${redColour} $ports${endColour}\n"
    echo $ports | tr -d '\n' | xclip -sel clip
    echo -e "${blueColour}[*] Ports copied to clipboard${endColour}\n"
}
```

Use the command to extract the **TCP ports**.

```java

```

We will see the open ports on the target machine so now, we are going to use [nmap](https://nmap.org/) again using the parameters below:

- [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
- [-sC](https://nmap.org/book/man-nse.html) : to performs a script scan using the default set of scripts.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- [-p](https://nmap.org/book/scan-methods-ip-protocol-scan.html) : to determine which IP protocols are supported by target machine.
- {name}.thm : Target machine.
- [-oN](https://nmap.org/book/output-formats-normal-output.html) : Normal output.
- targeted : Output file name.

```python

```

{: .box-error}

You can find the man page of nmap [here](https://nmap.org/book/toc.html).

## 1.2
