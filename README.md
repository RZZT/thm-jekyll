# Try Hack Me Write-ups

> Visit my new [blog](https://blog.raulsanchezzt.com).

This is the page where I used to post articles about **TryHackMe** rooms.


![](thm-home.png)

# Setup this web locally

1. Clone this repository

```bash
git@gitlab.com:RZZT/thm-jekyll.git

# or

https://gitlab.com/RZZT/thm-jekyll.git
```

2. Change the directory

```bash
cd thm-jekyll
```

3. Execute the `install.sh script`

```bash
./install.sh
```

4. Serve the website

```bash
bundle exec jekyll serve
```
